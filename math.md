## math for CS/CE

-   [Online latex Formula Editor](https://www.hostmath.com)
-   [Online Latex Equation Editor](https://latexeditor.lagrida.com)
[Opedia](https://opedia.ir/%D8%A2%D9%85%D9%88%D8%B2%D8%B4/%D9%81%D9%87%D8%B1%D8%B3%D8%AA) - [Archive](http://web.archive.org/web/20210104180023/https://opedia.ir/%D8%A2%D9%85%D9%88%D8%B2%D8%B4/%D9%81%D9%87%D8%B1%D8%B3%D8%AA)

[Shazzz book](https://gtoi.shaazzz.ir) - [Archive](http://web.archive.org/web/20210104183021/https://gtoi.shaazzz.ir/)
[Where to start Computer Olympiad](https://wiki.shaazzz.ir/wiki/%D8%A7%D9%84%D9%85%D9%BE%DB%8C%D8%A7%D8%AF_%DA%A9%D8%A7%D9%85%D9%BE%DB%8C%D9%88%D8%AA%D8%B1_%D8%B1%D8%A7_%D8%A7%D8%B2_%DA%A9%D8%AC%D8%A7_%D8%B4%D8%B1%D9%88%D8%B9_%DA%A9%D9%86%D9%85) - [Archive](http://web.archive.org/web/20210104183502/https://wiki.shaazzz.ir/wiki/%D8%A7%D9%84%D9%85%D9%BE%DB%8C%D8%A7%D8%AF_%DA%A9%D8%A7%D9%85%D9%BE%DB%8C%D9%88%D8%AA%D8%B1_%D8%B1%D8%A7_%D8%A7%D8%B2_%DA%A9%D8%AC%D8%A7_%D8%B4%D8%B1%D9%88%D8%B9_%DA%A9%D9%86%D9%85)


#### Distance Functions:

[source blog](https://blog.faradars.org/distance-functions/)

-      Euclidean Distance/فاصله اقلیدسی

one dimension:
<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/7d75418dbec9482dbcb70f9063ad66e9cf7b5db9" class="mwe-math-fallback-image-display" aria-hidden="true" style="vertical-align: -1.671ex; width:20.31ex; height:4.843ex;" alt="{\displaystyle d(p,q)={\sqrt {(p-q)^{2}}}.}">

two dimension:
<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/9c0157084fd89f5f3d462efeedc47d3d7aa0b773" class="mwe-math-fallback-image-display" aria-hidden="true" style="vertical-align: -1.671ex; width:35.245ex; height:4.843ex;" alt="{\displaystyle d(p,q)={\sqrt {(p_{1}-q_{1})^{2}+(p_{2}-q_{2})^{2}}}.}">


<img src="https://upload.wikimedia.org/wikipedia/commons/5/55/Euclidean_distance_2d.svg" class="mwe-math-fallback-image-display" aria-hidden="true" style="vertical-align: -1.671ex; width:35.245ex; height:4.843ex;" alt="{\displaystyle d(p,q)={\sqrt {(p_{1}-q_{1})^{2}+(p_{2}-q_{2})^{2}}}.}">



<script type="math/tex; mode=display" id="MathJax-Element-7">D_{euc}=(\sum_{i=1}^p(x_i-y_i)^2)^\tfrac{1}{2}</script>


-      [Manhattan Distance](https://en.wikipedia.org/wiki/Taxicab_geometry)/فاصله منهتن



<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/02436c34fc9562eb170e2e2cfddbb3303075b28e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:35.654ex; height:6.843ex;" alt="d_{1}(\mathbf {p} ,\mathbf {q} )=\|\mathbf {p} -\mathbf {q} \|_{1}=\sum _{i=1}^{n}|p_{i}-q_{i}|,">


-      Minkowski Distance/فاصله مینکوفسکی



[other sources](https://www.cut-the-knot.org/pythagoras/DistanceFormula.shtml)
