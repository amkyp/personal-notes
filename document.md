### Must Check

-   [Tech Community Comics](https://xkcd.com/1810/)
-   [empty half of glass in softwares](http://harmful.cat-v.org/software/)
-   [Reactive Programming Concept](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)
-   [Git Shit's to consider](https://ohshitgit.com)
-   [Computer Science first Topics](https://www.cs.utah.edu/~germain/PPS/Topics/)
-   [Unix 6 online Simulator](http://pdp11.aiju.de) - [faq](http://aiju.de/code/pdp11/faq)
-   [Pytudes](https://github.com/norvig/pytudes#pytudes-index-of-jupyter-ipython-notebooks)
-   [Algorithm Visualizations](https://visualgo.net/en/graphds)
-   [Algorithms](https://www.hackerearth.com/practice/algorithms)
-   [Data Structure Visualizations](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)
-   [Dictionary of Algorithms and Data Structures](https://xlinux.nist.gov/dads/)


### Must Take Care

-   [Capture a web page as it appears now for use as a trusted citation in the future](https://web.archive.org/save)
-   [Mozilla Voice Dataset](https://voice.mozilla.org/fa/datasets)
-   [Teach your self Computer Science](https://teachyourselfcs.com)
-   [Computer Science courses with video lectures](https://github.com/Developer-Y/cs-video-courses)
-   [جايگزين ياب](https://alternative.pdcommunity.ir)

---

### Try this

- [Ascii Camera](https://github.com/idevelop/ascii-camera)

---

### Database Modeling

-   [Database Markup Language](https://www.dbml.org/home/#intro)
-   [DBML e.g. \[dbdiagram.io\]](https://dbdiagram.io/d/5d5cb582ced98361d6ddc5ab)
-   [wwwsqldesigner](https://github.com/ondras/wwwsqldesigner)

---

### Structure and Interpretation of Computer Programs by Harold Abelson (Book)

- [link to publisher provided book](https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book.html)
- Most Important Concepts in CS
- e.g. Syntactic sugar

-   Psudo Code
```python
    array.forEach  # this one
    for x in array # insted of this.

```

-   Technical Implementation
```python
function forEach(array, callbackOnEachElement) {
  for(const element of array)
    callbackOnEachElement(element)
}
```

---


### The Land of Lisp (Book)

-   To understand System Design and Procedural Programming.
-   To understand the concept of Abstraction.
-   Know where and to what extent you need Abstraction and how; with rich abstractions, you can go from 3 simple code lines to an impeccable mathematical operation.

---

### Canonical Fucks (Decending Order)

-   [Gnome](https://www.gnome.org)   
-   [Netplan](https://netplan.io)
-   [How to take back control of /etc/resolv.conf on Linux](https://www.ctrl.blog/entry/resolvconf-tutorial.html)
-   [NGrok](https://ngrok.com)

I'll write about them.

---

### Not Completed Yet

Q: Is it possible to use a spare pc to run a VM? Like I have my main pc, which is the main one, I would like to run a few VM without lagging my main pc down, using a few spare pcs.
Without having them "exposed" when turned off, Basically just hot booting, I guess, with easy access from my main pc, Like a Cloud VM.

```
Answers:

  -	Hyper v server.
  -	Proxmox:
  It's a virtualization server that is free and super simple to set up.
  You can access it from a web browser or directly over SSH, VNC, or whatever you prefer.
  -	KVM Hypervisor
  One preferred setup is running the KVM hypervisor and then VNC to it, which is reasonably easy to setup. Especially when using Proxmox as the OS.
  -	Parsec/Nvidia Remote Play
  If you passthrough a GPU, you can use something like Parsec or Nvidia remote play (See moonlight stream) for very low latency and good quality video.
```

- Someone would say, I wonder if I run a Bulletproof hosting on my spare pc, so there you go and I have no Idea. [\[1\]](https://us.norton.com/internetsecurity-emerging-threats-what-is-bulletproof-hosting.html)[\[2\]](https://en.wikipedia.org/wiki/Bulletproof_hosting)

##### Denote that you shouldn't do illegal things when there is a free alternative.

---

### Not Completed Yet

- [Icecast](https://github.com/xiph/Icecast-Server/issues)
- [Slides](./multimedia-slides)

```
shoutcast
RMTP Server
RSTP Server
Video Codecs
```

---

`nload` – Monitor Linux Network Bandwidth Usage in Real Time

---

Many subs of this kind are more like: Here is a problem/hypothesis/theory/solution -> discuss!

Stack exchanges are more oriented towards: Here is my problem/question -> solution.

---


### Timer Interupter - Question

**Q:** Is it possible to write a program that has elapsed 1 second after the last 1 second? (It is necessary to periodically say every 1 second after the execution of our program that 1 second is gone and not using system time.)

- AVR is the best choise unlike Intel or even arm.
- Using an ASIC Clock Digital Circuit would be fine though.
- AVR is a 8MHZ Crystal, You see, even the AVR crystal lags back and forth, but not enough to interfere with sensitive tasks.


- [Real Time Clock(DS1307) with AVR](https://exploreembedded.com/wiki/Real_Time_Clock(DS1307)_with_AVR)


- [RTS - RealTime Systems](https://en.wikipedia.org/wiki/Real-time_computing)
- We could further and that would be **Near Realtime Systems**


[Astable Multivibrator and Astable Oscillator Circuit](https://www.electronics-tutorials.ws/waveforms/astable.html)

- This is very interesting; if we add a few inductors to it, it can give an output of 60 volts from a 6-volt input, and then it can be used to build an induction heater or even a powerful generator.

- Another idea would be using an approximation

Maybe you would have think of this first:
```python
import time

while True:
    print(f'\r{time.time()}', end='')
    time.sleep(0.9)
# Credits go to https://gitlab.com/DarkSuniuM
```

but it's using system time, though we needed execution time, and there we go


```python
import datetime as dt
get_now = dt.datetime.utcnow
start = get_now()
while True:
    print(f'\r{get_now() - start}', end='')
    time.sleep(0.99)
# Credits go to https://gitlab.com/DarkSuniuM
```

Another way is through Profiling 
```python
https://martinheinz.dev/blog/13
# Credits go to https://gitlab.com/frowzyispenguin
```

In Ruby
```ruby
#!/usr/bin/ruby -e 

sleep 1 
puts "one seconds passed"
# Credits go to https://gitlab.com/prp-e
```

In Bash
```bash
sleep 1
echo "1 sec passed"
# Credits go to https://gitlab.com/the-make-file
```

---

### Chat Over SSH

-   https://github.com/shazow/ssh-chat]

---

### Project Managment 

Tools with nesting tasks priority feature.


-   Oracle Primavera P6
-   Microsoft Project
-   Kanban
-   [clickup](https://clickup.com)
-   [checkvist](https://checkvist.com)
-   [workflowy](https://workflowy.com)
-   [worksection](https://worksection.com)
-   [toodledo](https://www.toodledo.com)

[edit]Save Addresses using [Archive Machine](web.archive.org/save)[/edit]

---

-   Search Under specefic directory in Archive.org
```
https://web.archive.org/web/*/https://courses.engr.illinois.edu/ece438/fa2019/*
```

---

- slack useful commands

```bash
/giphy [search term]
example: /giphy computers
/poll <question> <options...> [anonymous] [limit 1]
example: /poll "How are you?" "Good" "OK" "Bad" anonymous
/dm @user <msg>
example: /dm @pomcol What up
/remind [@someone | #channel] [what] [when]
example: /remind #csci140po-fall20 Checkpoint1 "Sep 9"
/shrug <msg>
example: /shrug No idea...
/anon #channel <msg>
example: /anon #csci140po-fall20 AHHHHHH...
/box
/collapse
/expand
```

---

**Not Completed Yet**

```
Q: What is used when we do not cut and connect the power supply mechanically?
```
```
Transistors, inductors, etc.
In general, see the same SMTP Relay that comes and takes the received email from one server and sends it to another server.
```
---

**grub not loading?**

```bash
# GNU GRUB

grub> ls
# some dirs will be shown

grub> set normal=("{{you should select one of tuples from `ls` relesul}}") 

grub> set prefix=(hd2,gpt3)/boot/grub/

grub> insmod normal
grub> normal
```

---


- https://bigbluebutton-exporter.greenstatic.dev/
- https://github.com/greenstatic/bigbluebutton-exporter/


---

- install zabbix 5

```bash
sudo apt update
sudo apt dist-upgrade
sudo reboot
wget https://repo.zabbix.com/zabbix/5.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.2-1+ubuntu20.04_all.deb
sudo dpkg -i zabbix-release_5.2-1+ubuntu20.04_all.deb
sudo apt update
sudo apt dist-upgrade
sudo systemctl restart zabbix-server
sudo systemctl restart zabbix-server
sudo systemctl restart nginx
sudo systemctl restart php7.4-fpm
sudo systemctl enable php7.4-fpm
```

---

# Should edit


NLP-982-ut:

https://drive.google.com/drive/folders/1yy_P3c7Gh-fIf4QWnYwgG_Cn4ZAHEXfQ

Discrete Math:

http://ocw.sharif.edu/course/id//58/-%D8%B1%DB%8C%D8%A7%D8%B6%DB%8C%D8%A7%D8%AA-%DA%AF%D8%B3%D8%B3%D8%AA%D9%87.html

Self-Study courses AI:

https://github.com/ZoeyCui0718/Self-Study/blob/4c63e83c8febfd1179ae4e61f55948b922770a6c/README.md


Statics 110: Probability 

https://projects.iq.harvard.edu/stat110/youtube

https://www.youtube.com/playlist?list=PL2SOU6wwxB0uwwH80KTQ6ht66KWxbzTIo

Search -> heartbeat linux
