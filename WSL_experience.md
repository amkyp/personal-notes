`WSL 1 -> WSL 2`

-   Upgrading to WSL-2 wasn't that easy, it requires to purge all the previous installations beside the Linux OS installed on top of Hyperv. 


-   Windows is handling virtual machin on top of HyperV, or better to say Microsoft planned to make HyperV handle this all.
-   Sth like OpenVZ Containers but not it was just like a small application inside Hyperv.

-   there was a tool called `virt-what`.
-   That I think i could be an equvalent to `dmidecode -t system` or `lsb_release -a` or `cat /etc/redhat-release` or even `hostnamectl`.


#### How to disable HyperV - you have to reboot after these commands

cmd
-       bcdedit /set hypervisorlaunchtype off

powershell
-       Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-Hypervisor

-       Control Panel -> select Programs and Features -> Select Turn Windows features on or off -> Expand Hyper-V -> expand Hyper-V Platform -> and then clear the Hyper-V Hypervisor check box.


<p dir="rtl">
برنامه‌های گرافیکی رو نمیشه بالا آورد مگر اینکه خودت یه سرور xorg ویندوزی نصب کنی مثل Cygwin/X
روی WSL1 نمیشد ولی روی WSL2 میشه که برنامه‌هایی که به کرنل لینوکس وابسته‌ان و نیاز به درایور خاصی دارن رو بالا بیاری
اکثر برنامه‌ها کار میکنن، میتونی سرچ کنی و یه لیست از برنامه‌هایی که روی WSL کار نمیکنن رو ببینی
</p>



-   Other `virt-what` possible values
                
        - hyperv : This is Microsoft Hyper-V hypervisor.
        - parallels : The guest is running inside Parallels Virtual Platform (Parallels Desktop, Parallels Server).
        - powervm_lx86 : The guest is running inside IBM PowerVM Lx86 Linux/x86 emulator.        
        - qemu : This is QEMU hypervisor using software emulation.
        - virtualpc : The guest appears to be running on Microsoft VirtualPC.
        - xen-hvm : This is a Xen guest fully virtualized (HVM).
        - uml : This is a User-Mode Linux (UML) guest.
        - openvz : The guest appears to be running inside an OpenVZ or Virtuozzo container.
        - linux_vserver : This process is running in a Linux VServer container.
        - ibm_systemz : This is an IBM SystemZ (or other S/390) hardware partitioning system.




---


Kernel: Neighbour table overflow (cam table flodding - related to arp)
Linux:

https://e-rave.nl/kernel-neighbour-table-overflow

windows:

netsh interface ipv4 show global
netsh interface ipv6 show global

Gives 256 on desktop Windows and 1024 on a 2012 server box.

To change:

netsh interface ipv4 set global neighborcachelimit <limit>
netsh interface ipv6 set global neighborcachelimit <limit>
