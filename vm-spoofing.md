Here would be some note on how I done VM Spoofing.
-   [a0rtega - pafish] - [1]
-   [Intercepting RDTSC instruction in KVM] - [2]
-   [hiding-virtual-machine-status-from-guest-operating-system] - [3]
-   [Spoof and make your vm undetectable no more] - [4]
-   [Link to Pastebin] - [5]


[a0rtega - pafish]: https://www.reddit.com/r/VFIO/comments/hts1o1/be_is_banning_kvm_on_r6/
[1]:  https://web.archive.org/web/20201124024211/https://github.com/a0rtega/pafish/  
[Intercepting RDTSC instruction in KVM]: https://www.reddit.com/r/VFIO/comments/hts1o1/be_is_banning_kvm_on_r6/
[2]:   https://web.archive.org/web/20201124023749/https://stackoverflow.com/questions/62970242/intercepting-rdtsc-instruction-in-kvm 

[hiding-virtual-machine-status-from-guest-operating-system]: https://superuser.com/questions/1387935/hiding-virtual-machine-status-from-guest-operating-system
[3]:   https://web.archive.org/web/20201124014833/https://superuser.com/questions/1387935/hiding-virtual-machine-status-from-guest-operating-system 

[Spoof and make your vm undetectable no more]: https://www.reddit.com/r/VFIO/comments/i071qx/spoof_and_make_your_vm_undetectable_no_more/
[4]:   https://web.archive.org/web/20201124023221/https://www.reddit.com/r/VFIO/comments/i071qx/spoof_and_make_your_vm_undetectable_no_more
[Link to Pastebin]: https://pastebin.com/90V2cL6K
[5]:     https://web.archive.org/web/20201124001925/https://pastebin.com/90V2cL6K
